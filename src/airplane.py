class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.number_of_seats = number_of_seats
        self.travel_distance = 0
        self.number_of_busy_seats = 0

    def fly(self, new_distance):
        self.travel_distance += new_distance

    def is_service_required(self):
        return self.travel_distance > 10000

    def board_passengers(self, number_of_passengers):
        total_number_of_passengers = self.number_of_busy_seats + number_of_passengers
        if total_number_of_passengers < self.number_of_seats:
            self.number_of_busy_seats = total_number_of_passengers
        else:
            self.number_of_busy_seats = self.number_of_seats

    def get_available_seats(self):
        return self.number_of_seats - self.number_of_busy_seats


if __name__ == '__main__':
    plane1 = Airplane('Camper', 200)
    plane2 = Airplane('Rester', 300)
    plane1.fly(1500)
    plane2.fly(30000)
    print(plane1.is_service_required())
    print(plane2.is_service_required())
    plane1.board_passengers(30)
    print(plane1.get_available_seats())
    plane2.board_passengers(300)
    print(plane2.get_available_seats())
